use serde_json::Value;

pub fn resolve_headers(entries: &Vec<Value>) -> Vec<String> {
    let mut headers: Vec<String> = Vec::new();

    for entry in entries.iter() {
        let object = entry.as_object().unwrap();
        for (key, _value) in &*object {
            if !headers.contains(key) {
                headers.push(key.to_string())
            }
        }
    }
    headers
}

fn create_row(entry: &Value, headers: &Vec<String>) -> Vec<String> {
    let mut row = vec![String::new(); headers.len()];
    let object = entry.as_object().unwrap();

    for (key, value) in &*object {
        let mut header_position = 0;
        for (index, header) in headers.iter().enumerate() {
            if header.eq(key) {
                header_position = index;
            }
        }
        row[header_position] = value.to_string();
    }
    return row;
}

pub fn create_rows(entries: &Vec<Value>, headers: &Vec<String>) -> Vec<Vec<String>> {
    let mut rows: Vec<Vec<String>> = Vec::new();

    for entry in entries.iter() {
        rows.push(create_row(entry, &headers));
    }
    return rows;
}