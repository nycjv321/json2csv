extern crate serde_json;

use std::io;
use std::io::BufRead;
mod json_to_csv;
mod parser;

fn read_input() -> String {
    let mut line = String::new();
    let stdin = io::stdin();
    stdin.lock().read_line(&mut line).expect("Could not read line");
    return line;
}

fn main() {
    let csv = json_to_csv::from_json(&read_input());
    csv.write_to(io::stdout());
}