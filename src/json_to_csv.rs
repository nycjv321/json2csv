extern crate csv;

extern crate serde_json;
use parser;
use std::io;
use self::csv::WriterBuilder;
use self::csv::QuoteStyle;

pub fn from_json(input: &str) -> CSV {
    let entries: serde_json::Value = serde_json::from_str(input).unwrap();
    let entries_as_values = &entries.as_array().unwrap();
    let headers = parser::resolve_headers(entries_as_values);
    let rows = parser::create_rows(entries_as_values, &headers);
    return CSV { headers, rows };
}

pub struct CSV {
    headers: Vec<String>,
    rows: Vec<Vec<String>>,
}

impl CSV {
    pub fn write_to<W: io::Write>(&self, w: W) {
        let mut wtr = WriterBuilder::new().quote_style(QuoteStyle::Never).double_quote(false).from_writer(w);
        wtr.write_record(&self.headers).expect("error writing headers");
        for row in &self.rows {
            wtr.write_record(row).expect("error writing record");
        }
        wtr.flush().expect("unable to flush writer");
      }
}


#[cfg(test)]
mod test {
    use json_to_csv::from_json;

    #[test]
    fn from_parser() {
        let input = &"[{\"foo\" :\"bar\"}, {\"key\" : \"value\"}, {\"foo\" : \"meap\", \"key\" :\"value\"}]";

        let csv = from_json(input);


        let mut writer = vec![];
        csv.write_to(&mut writer);
        assert_eq!("foo,key\n\"bar\",\n,\"value\"\n\"meap\",\"value\"\n", String::from_utf8(writer).expect("conversion to succeed"));
    }

}