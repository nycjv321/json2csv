# json2csv

A simple JSON to CSV converter. It reads from stdin and writes to stdout.

## JSON Structure

The input JSON must be an array of objects. See example below. Each object 
becomes it's own row. `json2csv` will dynamically generate the first header 
for you based on all object keys.

## Example Usage
    
    echo '[{"foo" :"bar"}, {"key" : "value"}, {"foo" : "meap", "key" :"value"}]' | ./json-2-csv 
    
    foo,key
    "bar",
    ,"value"
    "meap","value"

## Docker

### Building

    docker build -t json2csv .

### Running

    echo '[{"foo" :"bar"}, {"key" : "value"}, {"foo" : "meap", "key" :"value"}]' | docker run --rm  -i json2csv
    
    foo,key
    "bar",
    ,"value"
    "meap","value"
