FROM rust:1.23.0
WORKDIR /usr/src/json2csv
COPY . .
RUN cargo install
CMD ["json2csv"]
